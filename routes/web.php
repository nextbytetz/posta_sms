<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*
 * Frontend Routes
 * Namespaces indicate folder structure
 */

Route::group(['namespace' => 'Frontend', 'as' => 'frontend.'], function () {
    includeRouteFiles(__DIR__.'/Frontend/');
});

includeRouteFiles(__DIR__.'/Backend/');

Route::get('/debug', function () {
    return view('debug');
});

Route::get('user/profile', function() {
    return view('');
})->name('profile');


Route::post('/debug', function () {
    //echo 'ya it works';
   return view('debug');
});