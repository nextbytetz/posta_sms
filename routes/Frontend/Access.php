<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 27-Feb-17
 * Time: 8:36 PM
 */
Route::group(['namespace' => 'Auth', 'as' => 'auth.'], function () {
    Route::group(['middleware' => 'guest'], function () {
// Authentication Routes
        Route::get('login', 'LoginController@showLoginForm')->name('login');
        Route::post('login', 'LoginController@login');
        Route::get('register', 'RegisterController@showRegistrationForm')->name('register');
        Route::post('register', 'RegisterController@register')->name('register');
    });


    Route::group(['middleware' => 'auth'], function () {
        Route::get('logout', 'LoginController@logout')->name('logout');


    });

});