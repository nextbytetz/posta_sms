<?php

return [
    'login' => [
        'title' => 'Login',
        'username' => 'Username',
        'placeholder_name' => 'Enter username',
    ],
    'register' => [
        'title' => 'Register',
    ],
];