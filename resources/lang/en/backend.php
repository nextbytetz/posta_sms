<?php

return [
    'dashboard' => [
        'title' => 'Dashboard',
        'username' => 'Username',
        'placeholder_name' => 'Enter username',
    ],
    'register' => [
        'title' => 'Register',
    ],
];