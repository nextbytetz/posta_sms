@extends('frontend.layouts.master', ['title' => ' - ' . trans('frontend.register.title')])

@section('content')
    <div class="login-panel panel panel-default plain animated bounceIn">

        <!-- Start .panel -->
        <div class="panel-body">
            @include('includes.partials.messages')
            {{--<form class="form-horizontal mt0" action="#" id="register-form" role="form">--}}
                {{ Form::open(['url' => 'register', 'class' => 'form-horizontal mt0', 'id'=>'register-form', 'role'=>'form']) }}

            <div class="form-group">
                <div class="col-md-12">
                    <!-- col-md-12 start here -->
                    {{--<label for="">Email:</label>--}}
                    {!! Form::label('name', 'Name:') !!}
                </div>
                <!-- col-md-12 end here -->
                <div class="col-lg-12">
                    <div class="input-group input-icon">
                        {{--<input type="text" name="email" id="email" class="form-control" placeholder="Type your email ...">--}}
                        {!! Form::input('text', 'name', null, ['class' => 'form-control', 'placeholder' => 'Type your name ...']) !!}
                        {!! $errors->first('name', '<span class="help-block label label-danger">:message</span>') !!}

                        <span class="input-group-addon"><i class="fa fa-envelope s16"></i></span>
                    </div>
                </div>
            </div>

                <div class="form-group">
                    <div class="col-md-12">
                        <!-- col-md-12 start here -->
                        {{--<label for="">Email:</label>--}}
                        {!! Form::label('email', 'Email:') !!}
                    </div>
                    <!-- col-md-12 end here -->
                    <div class="col-lg-12">
                        <div class="input-group input-icon">
                            {{--<input type="text" name="email" id="email" class="form-control" placeholder="Type your email ...">--}}
                            {!! Form::input('text', 'email', null, ['class' => 'form-control', 'placeholder' => 'Type your email ...']) !!}
                            {!! $errors->first('email', '<span class="help-block label label-danger">:message</span>') !!}

                            <span class="input-group-addon"><i class="fa fa-envelope s16"></i></span>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-12">
                        <!-- col-md-12 start here -->
                        {{--<label for="">Password:</label>--}}
                        {!! Form::label('password', 'Password:') !!}
                    </div>
                    <!-- col-md-12 end here -->
                    <div class="col-lg-12">
                        <div class="input-group input-icon">
                            {{--<input type="password" name="password" id="password" class="form-control" placeholder="Your password">--}}
                            {!! Form::input('password', 'password', null, ['class' => 'form-control', 'placeholder' => 'Type your password ...']) !!}
                            {!! $errors->first('password', '<span class="help-block label label-danger">:message</span>') !!}
                            <span class="input-group-addon"><i class="icomoon-icon-key s16"></i></span>
                        </div>
                        <div class="input-group input-icon">
                            {{--<input type="password" name="password1" id="password1" class="form-control" placeholder="Repeat password">--}}
                            {!! Form::input('password', 'password_confirmation', null, ['class' => 'form-control', 'placeholder' => 'Type again your password ...']) !!}
                            <span class="input-group-addon"><i class="icomoon-icon-key s16"></i></span>
                        </div>
                    </div>
                </div>
                <div class="form-group mb0">
                    <div class="col-md-12">
                        {{--<button class="btn btn-default" type="submit">Register</button>--}}
                        {!! Form::button('Register', ['class' => 'btn btn-default', 'type'=>'submit']) !!}
                    </div>
                </div>
            {{--</form>--}}
            {{ Form::close() }}
        </div>
        <div class="panel-footer gray-lighter-bg">
            <h4 class="text-center"><strong>Already have an account ?</strong>
            </h4>
            <p class="text-center"><a href="{!! route('frontend.auth.login') !!}" class="btn btn-primary">Sign in</a>
            </p>
        </div>
    </div>
    <!-- End .panel -->
@stop