@extends('frontend.layouts.master', ['title' => ' - ' . trans('frontend.login.title')])

@section('content')
    <div class="login-panel panel panel-default plain animated bounceIn">
        <!-- Start .panel -->
        <div class="panel-body">
            @include('includes.partials.messages')
         {{--<form class="form-horizontal mt0" action="/debug" id="login-form" role="form" method="post">--}}
         {{ Form::open(['route' => 'frontend.auth.login', 'class' => 'form-horizontal mt0','id'=> "login-form", 'role'=> "form" ]) }}

                <div class="form-group">
                    <div class="col-md-12">
                        <!-- col-md-12 start here -->
                        {{--<label for=""> {!! trans('frontend.login.username') !!}</label>--}}
                        {!! Form::label('email', 'Email:') !!}
                    </div>
                    <!-- col-md-12 end here -->
                    <div class="col-lg-12">
                        <div class="input-group input-icon">
                            {{--<input type="text" name="username" id="username" class="form-control"  placeholder="{!! trans('frontend.login.placeholder_name') !!}">--}}
                            {!! Form::input('text', 'email', null, ['class' => 'form-control', 'placeholder' => 'Type your email ...']) !!}
                            {!! $errors->first('email', '<span class="help-block label label-danger">:message</span>') !!}
                            <span class="input-group-addon"><i class="icomoon-icon-user s16"></i></span>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-12">
                        <!-- col-md-12 start here -->
                        {{--<label for="">Password:</label>--}}
                        {!! Form::label('password', 'Password:') !!}
                    </div>
                    <!-- col-md-12 end here -->
                    <div class="col-lg-12">
                        <div class="input-group input-icon">
                            {{--<input type="password" name="password" id="password" class="form-control" placeholder="Your password">--}}
                            {!! Form::input('password', 'password', null, ['class' => 'form-control', 'placeholder' => 'Type your password ...']) !!}
                            {!! $errors->first('password', '<span class="help-block label label-danger">:message</span>') !!}

                            <span class="input-group-addon"><i class="icomoon-icon-lock s16"></i></span>
                        </div>
                        <span class="help-block text-right"><a href="lost-password.html">Forgout password ?</a></span>
                    </div>
                </div>
                <div class="form-group mb0">
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-8">
                        <div class="checkbox-custom">
                            {{--<input type="checkbox" name="remember" id="remember" value="option">--}}
                            {!! Form::input('checkbox', 'remember') !!}
                            {{--<label for="remember">Remember me ?</label>--}}
                            {!! Form::label('remember', 'Remember me?') !!}
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-4 mb25">
                        {{--<button class="btn btn-default pull-right" type="submit">Login</button>--}}
                        {!! Form::button('Login', ['class' => 'btn btn-default pull-right', 'type'=>'submit']) !!}
                    </div>
                </div>
                {{--</form>--}}
                {{ Form::close() }}
            {{--<div class="seperator">--}}
                {{--<strong>or</strong>--}}
                {{--<hr>--}}
            {{--</div>--}}
            {{--<div class="social-buttons text-center mt5 mb5">--}}
                {{--<a href="#" class="btn btn-primary btn-alt mr10">Sign in with <i class="fa fa-facebook s20 ml5 mr0"></i></a>--}}
                {{--<a href="#" class="btn btn-danger btn-alt ml10">Sign in with <i class="fa fa-google-plus s20 ml5 mr0"></i></a>--}}
            {{--</div>--}}
        </div>
        <div class="panel-footer gray-lighter-bg">
            <h4 class="text-center"><strong>Don`t have an account ?</strong>
            </h4>
            <p class="text-center"><a href="{!! route('frontend.auth.register') !!}" class="btn btn-success">Create account</a>
            </p>
        </div>
    </div>
    <!-- End .panel -->
@stop

