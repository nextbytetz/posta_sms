<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from demos.getbootstrapkit.com/supr/login.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 21 Feb 2017 15:04:50 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->
<head>
    <meta charset="utf-8">
    <title>{{ env('WEB_NAME') . $title }}</title>
    <!-- Mobile specific metas -->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <!-- Force IE9 to render in normal mode -->
    <!--[if IE]><meta http-equiv="x-ua-compatible" content="IE=9" /><![endif]-->
    <meta name="author" content="" />
    <meta name="description" content="{!! env('DESCRIPTION') !!}" />
    <meta name="keywords" content="" />
    <meta name="application-name" content="{!! env('APPLICATION_NAME') !!}" />
    <!-- Import google fonts - Heading first/ text second -->
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,700" rel="stylesheet" type="text/css">
    <link href="http://fonts.googleapis.com/css?family=Droid+Sans:400,700" rel="stylesheet" type="text/css">
    <!-- Css files -->
    <!-- Icons -->
    <link href="{!! asset_url() !!}/css/icons.css" rel="stylesheet" />
    <!-- Bootstrap stylesheets (included template modifications) -->
    <link href="{!! asset_url() !!}/css/bootstrap.css" rel="stylesheet" />
    <!-- Plugins stylesheets (all plugin custom css) -->
    <link href="{!! asset_url() !!}/css/plugins.css" rel="stylesheet" />
    <!-- Main stylesheets (template main css file) -->
    <link href="{!! asset_url() !!}/css/main.css" rel="stylesheet" />
    <!-- Custom stylesheets ( Put your own changes here ) -->
    <link href="{!! asset_url() !!}/css/custom.css" rel="stylesheet" />
    <!-- Fav and touch icons -->
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="{!! asset_url() !!}/img/ico/apple-touch-icon-144-precomposed.html">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="{!! asset_url() !!}/img/ico/apple-touch-icon-114-precomposed.html">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="{!! asset_url() !!}/img/ico/apple-touch-icon-72-precomposed.html">
    <link rel="apple-touch-icon-precomposed" href="{!! asset_url() !!}/img/ico/apple-touch-icon-57-precomposed.html">
    <link rel="icon" href="{!! asset_url() !!}/img/ico/favicon.html" type="image/png">
    <!-- Windows8 touch icon ( http://www.buildmypinnedsite.com/ )-->
    <meta name="msapplication-TileColor" content="#3399cc" />
</head>
<body class="login-page">
<div id="header" class="animated fadeInDown">
    <div class="row">
        <div class="navbar">
            <div class="container text-center">
                <a class="navbar-brand" href="#">{!! env('WEB_NAME') !!}</a>
            </div>
        </div>
        <!-- /navbar -->
    </div>
    <!-- End .row -->
</div>
<!-- End #header -->
<!-- Start login container -->

<div class="container login-container">
    @yield('content')
</div>
<!-- End login container -->
<div class="container">
    <div class="footer">
        <p class="text-center">&copy;{!! date('Y') !!} Copyright Supr.admin. All right reserved !!!</p>
    </div>
</div>
<!-- Javascripts -->
<!-- Important javascript libs(put in all pages) -->
<script src="{!! asset_url() !!}/js/jquery-2.1.1.min.js"></script>
<script>
    window.jQuery || document.write('<script src="{!! asset_url() !!}/assets/js/libs/jquery-2.1.1.min.html">\x3C/script>')
</script>
<script src="{!! asset_url() !!}/js/ui/1.10.4/jquery-ui.js"></script>
<script>
    window.jQuery || document.write('<script src="{!! asset_url() !!}/assets/js/libs/jquery-ui-1.10.4.min.html">\x3C/script>')
</script>
<!--[if lt IE 9]>
<script type="text/javascript" src="{!! asset_url() !!}/js/libs/excanvas.min.js"></script>
<script type="text/javascript" src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
<script type="text/javascript" src="{!! asset_url() !!}/js/libs/respond.min.js"></script>
<![endif]-->
<!-- Bootstrap plugins -->
<script src="{!! asset_url() !!}/js/bootstrap/bootstrap.js"></script>
<!-- Form plugins -->
{{--<script src="{!! asset_url() !!}/plugins/forms/validation/jquery.validate.js"></script>--}}
{{--<script src="{!! asset_url() !!}/plugins/forms/validation/additional-methods.min.js"></script>--}}
<!-- Init plugins olny for this page -->
<script src="{!! asset_url() !!}/js/pages/login.js"></script>
</body>

<!-- Mirrored from demos.getbootstrapkit.com/supr/login.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 21 Feb 2017 15:04:53 GMT -->
</html>
