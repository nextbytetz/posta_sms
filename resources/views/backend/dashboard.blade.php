@extends('backend.layouts.master', ['title' => ' - ' . trans('backend.dashboard.title')])
@section('content')
        <!--Body content-->
<div id="content" class="page-content clearfix ">
    <div class="contentwrapper">
        <!--Content wrapper-->
        <div class="heading">
            <!--  .heading-->
            <h3>{!! trans('backend.dashboard.title') !!}</h3>
            <div class="resBtnSearch">
                <a href="#"><span class="s16 icomoon-icon-search-3"></span></a>
            </div>
            <div class="search">
                <!-- .search -->
                <form id="searchform" class="form-horizontal" action="http://demos.getbootstrapkit.com/supr/search.html">
                    <input type="text" class="top-search from-control" placeholder="Search here ..." />
                    <input type="submit" class="search-btn" value="" />
                </form>
            </div>
            <!--  /search -->
            <ul class="breadcrumb">
                <li>You are here:</li>
                <li>
                    <a href="#" class="tip" title="back to dashboard">
                        <i class="s16 icomoon-icon-screen-2"></i>
                    </a>
                                <span class="divider">
                <i class="s16 icomoon-icon-arrow-right-3"></i>
            </span>
                </li>
                <li class="active">Blank Page</li>
            </ul>
        </div>
        <!-- End  / heading-->
        <!-- Start .row -->
        <div class="row">
        </div>
        <!-- End .row -->
    </div>
    <!-- End contentwrapper -->
</div>
<!-- End #content -->
@stop