<!doctype html>
<!--[if lt IE 8]><html class="no-js lt-ie8"> <![endif]-->
<html class="no-js">

<!-- Mirrored from demos.getbootstrapkit.com/supr/ by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 21 Feb 2017 15:01:42 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->
<head>
    <meta charset="utf-8">
    <title>{{ env('WEB_NAME') . $title }}</title>
    <!-- Mobile specific metas -->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <!-- Force IE9 to render in normal mode -->
    <!--[if IE]><meta http-equiv="x-ua-compatible" content="IE=9" /><![endif]-->
    <meta name="author" content="" />
    <meta name="description" content="" />
    <meta name="keywords" content="" />
    <meta name="application-name" content="" />
    <!-- Import google fonts - Heading first/ text second -->
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,700" rel="stylesheet" type="text/css">
    <link href="http://fonts.googleapis.com/css?family=Droid+Sans:400,700" rel="stylesheet" type="text/css">
    <!-- Css files -->
    <!-- Icons -->
    <link href="{!! asset_url() !!}/css/icons.css" rel="stylesheet" />
    <!-- Bootstrap stylesheets (included template modifications) -->
    <link href="{!! asset_url() !!}/css/bootstrap.css" rel="stylesheet" />
    <!-- Plugins stylesheets (all plugin custom css) -->
    <link href="{!! asset_url() !!}/css/plugins.css" rel="stylesheet" />
    <!-- Main stylesheets (template main css file) -->
    <link href="{!! asset_url() !!}/css/main.css" rel="stylesheet" />
    <!-- Custom stylesheets ( Put your own changes here ) -->
    <link href="{!! asset_url() !!}/css/custom.css" rel="stylesheet" />
    <!-- Fav and touch icons -->
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="{!! asset_url() !!}/img/ico/apple-touch-icon-144-precomposed.html">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="{!! asset_url() !!}/img/ico/apple-touch-icon-114-precomposed.html">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="{!! asset_url() !!}/img/ico/apple-touch-icon-72-precomposed.html">
    <link rel="apple-touch-icon-precomposed" href="{!! asset_url() !!}/img/ico/apple-touch-icon-57-precomposed.html">
    <link rel="icon" href="{!! asset_url() !!}/img/ico/favicon.html" type="image/png">
    <!-- Windows8 touch icon ( http://www.buildmypinnedsite.com/ )-->
    <meta name="msapplication-TileColor" content="#3399cc" />
</head>
<body>
<!--[if lt IE 9]>
<p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
<![endif]-->
<!-- #wrapper: sidebar + body + footer -->

@include('backend.layouts.header.header')
<div id="wrapper">
    @include('backend.layouts.sidebar.sidebar')

    @yield('content')
    @include('backend.layouts.footer.footer')
</div>
<!-- / #wrapper -->
<!-- Back to top -->
<div id="back-to-top"><a href="#">Back to Top</a>
</div>
<!-- Javascripts -->
<!-- Load pace first -->
<script src="{!! asset_url() !!}/plugins/core/pace/pace.min.js"></script>
<!-- Important javascript libs(put in all pages) -->
<script src="{!! asset_url() !!}/js/jquery-2.1.1.min.js"></script>
<script>
    window.jQuery || document.write('<script src="{!! asset_url() !!}/js/libs/jquery-2.1.1.min.js">\x3C/script>')
</script>
<script src="{!! asset_url() !!}/js/ui/1.10.4/jquery-ui.js"></script>
<script>
    window.jQuery || document.write('<script src="{!! asset_url() !!}/js/libs/jquery-ui-1.10.4.min.js">\x3C/script>')
</script>
<script src="{!! asset_url() !!}/js/jquery-migrate-1.2.1.min.js"></script>
<script>
    window.jQuery || document.write('<script src="{!! asset_url() !!}/js/libs/jquery-migrate-1.2.1.min.js">\x3C/script>')
</script>
<!--[if lt IE 9]>
<script type="text/javascript" src="{!! asset_url() !!}/js/libs/excanvas.min.js"></script>
<script type="text/javascript" src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
<script type="text/javascript" src="{!! asset_url() !!}/js/libs/respond.min.js"></script>
<![endif]-->
<!-- Bootstrap plugins -->
<script src="{!! asset_url() !!}/js/bootstrap/bootstrap.js"></script>
<!-- Core plugins ( not remove ) -->
<script src="{!! asset_url() !!}/js/libs/modernizr.custom.js"></script>
<!-- Handle responsive view functions -->
<script src="{!! asset_url() !!}/js/jRespond.min.js"></script>
<!-- Custom scroll for sidebars,tables and etc. -->
<script src="{!! asset_url() !!}/plugins/core/slimscroll/jquery.slimscroll.min.js"></script>
<script src="{!! asset_url() !!}/plugins/core/slimscroll/jquery.slimscroll.horizontal.min.js"></script>
<!-- Remove click delay in touch -->
<script src="{!! asset_url() !!}/plugins/core/fastclick/fastclick.js"></script>
<!-- Increase jquery animation speed -->
<script src="{!! asset_url() !!}/plugins/core/velocity/jquery.velocity.min.js"></script>
<!-- Quick search plugin (fast search for many widgets) -->
<script src="{!! asset_url() !!}/plugins/core/quicksearch/jquery.quicksearch.js"></script>
<!-- Bootbox fast bootstrap modals -->
<script src="{!! asset_url() !!}/plugins/ui/bootbox/bootbox.js"></script>
<!-- Other plugins ( load only nessesary plugins for every page) -->
<script src="{!! asset_url() !!}/plugins/charts/sparklines/jquery.sparkline.js"></script>
<script src="{!! asset_url() !!}/plugins/charts/knob/jquery.knob.js"></script>
<script src="{!! asset_url() !!}/plugins/charts/flot/jquery.flot.custom.js"></script>
<script src="{!! asset_url() !!}/plugins/charts/flot/jquery.flot.pie.js"></script>
<script src="{!! asset_url() !!}/plugins/charts/flot/jquery.flot.resize.js"></script>
<script src="{!! asset_url() !!}/plugins/charts/flot/jquery.flot.time.js"></script>
<script src="{!! asset_url() !!}/plugins/charts/flot/jquery.flot.growraf.js"></script>
<script src="{!! asset_url() !!}/plugins/charts/flot/jquery.flot.categories.js"></script>
<script src="{!! asset_url() !!}/plugins/charts/flot/jquery.flot.stack.js"></script>
<script src="{!! asset_url() !!}/plugins/charts/flot/jquery.flot.orderBars.js"></script>
<script src="{!! asset_url() !!}/plugins/charts/flot/jquery.flot.tooltip.min.js"></script>
<script src="{!! asset_url() !!}/plugins/ui/waypoint/waypoints.js"></script>
<script src="{!! asset_url() !!}/plugins/forms/autosize/jquery.autosize.js"></script>
<script src="{!! asset_url() !!}/js/jquery.supr.js"></script>
<script src="{!! asset_url() !!}/js/main.js"></script>
<script src="{!! asset_url() !!}/js/pages/dashboard.js"></script>
</body>

<!-- Mirrored from demos.getbootstrapkit.com/supr/ by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 21 Feb 2017 15:03:23 GMT -->
</html>