
    <!-- #wrapper -->
    <!--Sidebar background-->
    <div id="sidebarbg" class="hidden-lg hidden-md hidden-sm hidden-xs"></div>
    <!--Sidebar content-->
    <div id="sidebar" class="page-sidebar hidden-lg hidden-md hidden-sm hidden-xs">
        <div class="shortcuts">
            <ul>
                {{--<li><a href="support.html" title="Support section" class="tip"><i class="s24 icomoon-icon-support"></i></a>--}}
                {{--</li>--}}
                {{--<li><a href="#" title="Database backup" class="tip"><i class="s24 icomoon-icon-database"></i></a>--}}
                {{--</li>--}}
                {{--<li><a href="charts.html" title="Sales statistics" class="tip"><i class="s24 icomoon-icon-pie-2"></i></a>--}}
                {{--</li>--}}
                {{--<li><a href="#" title="Write post" class="tip"><i class="s24 icomoon-icon-pencil"></i></a>--}}
                {{--</li>--}}
                <li><!--Sidebar collapse button-->
                    <a href="#" class="collapseBtn leftbar"><i class="s16 minia-icon-list-3"></i></a>
                </li>
            </ul>
        </div>
        <!-- End search -->
        <!-- Start .sidebar-inner -->
        <div class="sidebar-inner">
            <!-- Start .sidebar-scrollarea -->
            <div class="sidebar-scrollarea">
                <div class="sidenav">
                    <div class="sidebar-widget mb0">
                        <h6 class="title mb0">Navigation</h6>
                    </div>
                    <!-- End .sidenav-widget -->
                    <div class="mainnav">
                        <ul>
                            <li><a href="index-2.html"><i class="s16 icomoon-icon-screen-2"></i><span class="txt">Dashboard</span> </a>
                            </li>

                            <li>
                                <a href="#"><i class="s16 icomoon-icon-users"></i><span class="txt">Subscriber</span> </a>
                                <ul class="sub">
                                    <li><a href="index-2.html"><i class="s16 icomoon-icon-user-plus"></i><span class="txt">Add New Subscriber</span> </a>
                                    </li>
                                    <li><a href="index-2.html"><i class="s16  icomoon-icon-users-2"></i><span class="txt">View Subscribers</span> </a>
                                    </li>
                                </ul>
                            </li>
                                  <li><a href="index-2.html"><i class="s16 icomoon-icon-folder-download"></i><span class="txt">Receive Item</span> </a>

                            </li>


                                  <li><a href="index-2.html"><i class="s16 icomoon-icon-mail-4"></i><span class="txt">Credit Top Up</span> </a>

                            </li>


                            <li>
                                <a href="#"><i class="s16 icomoon-icon-mail-4"></i><span class="txt">Bulk Sms</span> </a>
                                <ul class="sub">
                                    <li><a href="index-2.html"><i class="s16   icomoon-icon-checkmark-circle-2"></i><span class="txt">Load Received Items</span> </a>
                                    </li>
                                    <li><a href="index-2.html"><i class="s16  icomoon-icon-mail-3"></i><span class="txt">Update Pending Sms</span> </a>
                                    </li>

                                       <li><a href="index-2.html"><i class="s16   icomoon-icon-mail"></i><span class="txt">Send Bulk Sms</span> </a>
                                    </li>
                                       <li><a href="index-2.html"><i class="s16   icomoon-icon-mail-2"></i><span class="txt">Send Expiry Reminder</span> </a>
                                    </li>
                                </ul>
                            </li>

                              <li>
                                <a href="#"><i class="s16  icomoon-icon-users"></i><span class="txt">Users</span> </a>
                                <ul class="sub">
                                    <li><a href="index-2.html"><i class="s16  icomoon-icon-user-plus-2"></i><span class="txt">Add New User</span> </a>
                                    </li>
                                    <li><a href="index-2.html"><i class="s16   icomoon-icon-users-2"></i><span class="txt"> View Users</span> </a>
                                    </li>

                                </ul>
                 </li>
                                 {{--<p align="center"><img src="{!! asset_url() !!}/img/posta.jpg" alt="Posta Logo" class="image"  /></p>--}}


                        </ul>
                    </div>
                </div>


                <!-- End .sidenav-widget -->
            </div>
            <!-- End .sidebar-scrollarea -->
        </div>
        <!-- End .sidebar-inner -->
    </div>
    <!-- End #sidebar -->
<!--Sidebar background-->
{{--<div id="right-sidebarbg" class="hidden-lg hidden-md hidden-sm hidden-xs"></div>--}}
{{--<!-- Start #right-sidebar -->--}}
{{--<aside id="right-sidebar" class="right-sidebar hidden-lg hidden-md hidden-sm hidden-xs">--}}
    {{--<!-- Start .sidebar-inner -->--}}
    {{--<div class="sidebar-inner">--}}
        {{--<!-- Start .sidebar-scrollarea -->--}}
        {{--<div class="sidebar-scrollarea">--}}
            {{--<div class="pl10 pt10 pr5">--}}
                {{--<ul class="timeline timeline-icons">--}}
                    {{--<li>--}}
                        {{--<p>--}}
                            {{--<a href="#">Jonh Doe</a> attached new <a href="#">file</a>--}}
                            {{--<span class="timeline-icon"><i class="fa fa-file-text-o"></i></span>--}}
                            {{--<span class="timeline-date">Dec 10, 22:00</span>--}}
                        {{--</p>--}}
                    {{--</li>--}}
                    {{--<li>--}}
                        {{--<p>--}}
                            {{--<a href="#">Admin</a> approved <a href="#">3 new comments</a>--}}
                            {{--<span class="timeline-icon"><i class="fa fa-comment"></i></span>--}}
                            {{--<span class="timeline-date">Dec 8, 13:35</span>--}}
                        {{--</p>--}}
                    {{--</li>--}}
                    {{--<li>--}}
                        {{--<p>--}}
                            {{--<a href="#">Jonh Smith</a> deposit 300$--}}
                            {{--<span class="timeline-icon"><i class="fa fa-money color-green"></i></span>--}}
                            {{--<span class="timeline-date">Dec 6, 10:17</span>--}}
                        {{--</p>--}}
                    {{--</li>--}}
                    {{--<li>--}}
                        {{--<p>--}}
                            {{--<a href="#">Serena Williams</a> purchase <a href="#">3 items</a>--}}
                            {{--<span class="timeline-icon"><i class="fa fa-shopping-cart color-red"></i></span>--}}
                            {{--<span class="timeline-date">Dec 5, 04:36</span>--}}
                        {{--</p>--}}
                    {{--</li>--}}
                    {{--<li>--}}
                        {{--<p>--}}
                            {{--<a href="#">1 support </a> request is received from <a href="#">Klaudia Chambers</a>--}}
                            {{--<span class="timeline-icon"><i class="fa fa-life-ring color-gray-light"></i></span>--}}
                            {{--<span class="timeline-date">Dec 4, 18:40</span>--}}
                        {{--</p>--}}
                    {{--</li>--}}
                    {{--<li>--}}
                        {{--<p>--}}
                            {{--You received 136 new likes for <a href="#">your page</a>--}}
                            {{--<span class="timeline-icon"><i class="glyphicon glyphicon-thumbs-up"></i></span>--}}
                            {{--<span class="timeline-date">Dec 4, 12:00</span>--}}
                        {{--</p>--}}
                    {{--</li>--}}
                    {{--<li>--}}
                        {{--<p>--}}
                            {{--<a href="#">12 settings </a> are changed from <a href="#">Master Admin</a>--}}
                            {{--<span class="timeline-icon"><i class="glyphicon glyphicon-cog"></i></span>--}}
                            {{--<span class="timeline-date">Dec 3, 23:17</span>--}}
                        {{--</p>--}}
                    {{--</li>--}}
                    {{--<li>--}}
                        {{--<p>--}}
                            {{--<a href="#">Klaudia Chambers</a> change your photo--}}
                            {{--<span class="timeline-icon"><i class="icomoon-icon-image-2"></i></span>--}}
                            {{--<span class="timeline-date">Dec 2, 05:17</span>--}}
                        {{--</p>--}}
                    {{--</li>--}}
                    {{--<li>--}}
                        {{--<p>--}}
                            {{--<a href="#">Master server </a> is down for 10 min.--}}
                            {{--<span class="timeline-icon"><i class="icomoon-icon-database"></i></span>--}}
                            {{--<span class="timeline-date">Dec 2, 04:56</span>--}}
                        {{--</p>--}}
                    {{--</li>--}}
                    {{--<li>--}}
                        {{--<p>--}}
                            {{--<a href="#">12 links </a> are broken--}}
                            {{--<span class="timeline-icon"><i class="fa fa-unlink"></i></span>--}}
                            {{--<span class="timeline-date">Dec 1, 22:13</span>--}}
                        {{--</p>--}}
                    {{--</li>--}}
                    {{--<li>--}}
                        {{--<p>--}}
                            {{--<a href="#">Last backup </a> is restored by <a href="#">Master admin</a>--}}
                            {{--<span class="timeline-icon"><i class="fa fa-undo color-red"></i></span>--}}
                            {{--<span class="timeline-date">Dec 1, 17:42</span>--}}
                        {{--</p>--}}
                    {{--</li>--}}
                {{--</ul>--}}
                {{--<a href="#" class="btn btn-default timeline-load-more-btn"><i class="fa fa-refresh"></i> Load more </a>--}}
            {{--</div>--}}
        {{--</div>--}}
        {{--<!-- End .sidebar-scrollarea -->--}}
    {{--</div>--}}
    {{--<!-- End .sidebar-inner -->--}}
{{--</aside>--}}
<!-- End #right-sidebar -->