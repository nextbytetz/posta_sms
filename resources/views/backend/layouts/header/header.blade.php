<!- .#header -->
<div id="header">
    <nav class="navbar navbar-default" role="navigation">
        <div class="navbar-header">
            <a class="navbar-brand" href="index-2.html">
                Posta Sms:<span class="slogan">Delivery Messenger</span>
                            </a>
        </div>
        <div id="navbar-no-collapse" class="navbar-no-collapse">
            <ul class="nav navbar-nav">
                         </ul>
            <ul class="nav navbar-right usernav">

                <li class="dropdown">
                    <a href="" class="dropdown-toggle avatar" data-toggle="dropdown">
                        <img src="{!! asset_url() !!}/img/avatar.jpg" alt="" class="image" />
                        <span class="txt">{{ Auth::user()->email }}</span>
                        <b class="caret"></b>
                    </a>
                    <ul class="dropdown-menu right">
                        <li class="menu">
                            <ul>
                                <li><a href="{{ url('debug') }}"><i class="s16 icomoon-icon-user-plus"></i>Edit profile</a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </li>
                <li><a href="{!! route('frontend.auth.logout') !!}"><i class="s16 icomoon-icon-exit"></i><span class="txt"> Logout</span></a>
                </li>
                           </ul>
        </div>
        <!-- /.nav-collapse -->
    </nav>
    <!-- /navbar -->
</div>
<!-- / #header -->