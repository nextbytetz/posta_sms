<div id="footer" class="clearfix sidebar-page right-sidebar-page">
    <!-- Start #footer  -->
    <p class="pull-left">
        Copyrights &copy; {!! date('Y') !!} <a href="http://www.nextbyte.com/" class="color-blue strong" target="_blank">NextByte ICT Solutions</a>. All rights reserved.
    </p>
    <p class="pull-right">
        <a href="#" class="mr5">Terms of use</a>
        |
        <a href="#" class="ml5 mr25">Privacy police</a>
    </p>
</div>
<!-- End #footer  -->