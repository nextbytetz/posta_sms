<?php

namespace App\Http\Controllers\Frontend\Auth;

use App\Http\Requests\Frontend\Auth\LoginRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

/**
 * Class LoginController.
 */
class LoginController extends Controller
{
    use AuthenticatesUsers;

    public function showLoginForm()
    {
        return view('frontend.auth.login');
    }

    public function redirectPath()
    {

             //return view('backend.dashboard');
        return route('dashboard');
       // return view('backend.layouts.master');
      //  return view('/debug');
    }

    protected function authenticated(Request $request, $user)
    {
       return redirect()->intended($this->redirectPath());
        //return view('debug');
    }

    public function logout(Request $request)
    {

        /*
 * Laravel specific logic
 */
        $this->guard()->logout();
        $request->session()->flush();
        $request->session()->regenerate();

        return redirect('/');
    }

}
