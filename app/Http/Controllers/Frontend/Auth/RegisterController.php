<?php

namespace App\Http\Controllers\Frontend\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\RegistersUsers;
use App\Http\Requests\Frontend\Auth\RegisterRequest;
use App\Repositories\Frontend\UserRepository;

/**
 * Class RegisterController.
 */
class RegisterController extends Controller
{
    //use RegistersUsers;

    /**
     * @var UserRepository
     */
    protected $user;

    protected $redirectTo;

    /**
     * RegisterController constructor.
     *
     * @param UserRepository $user
     */
    public function __construct(UserRepository $user)
    {
        // Where to redirect users after registering
        $this->redirectTo = route('frontend.auth.login');
        $this->user = $user;
    }

    public function showRegistrationForm()
    {
        return  view('frontend.auth.register');
    }

    public function register(RegisterRequest $request)
    {
            $user = $this->user->create($request->all());
            return redirect($this->redirectTo)->withFlashSuccess("Successful Registered ...");
    }

}
