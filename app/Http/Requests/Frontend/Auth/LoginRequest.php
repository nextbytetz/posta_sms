<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 03-Mar-17
 * Time: 8:21 PM
 */
namespace App\Http\Requests\Frontend\Auth;

use App\Http\Requests\Request;
use Illuminate\Validation\Rule;

//class LoginRequest
class LoginRequest extends Request
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */

    public function rules()
    {
        return [
            'email'                => ['required', 'email', 'max:20'],
            'password'             => 'required',
        ];
    }




}