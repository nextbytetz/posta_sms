<?php

namespace App\Repositories\Frontend;

use App\Models\Access\User\User;
use App\Repositories\BaseRepository;

class UserRepository extends BaseRepository
{
    /**
     * Associated Repository Model.
     */
    const MODEL = User::class;

    public function create(array $data, $provider = false)
    {
        $user = self::MODEL;
        $user = new $user();
        $user->name = $data['name'];
        $user->email = $data['email'];
        $user->password = bcrypt($data['password']);
        $user->save();

        /*
         * Return the user object
         */
        return $user;
    }
}