<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RestructuringDatabase extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::rename("charge", "charges");
        Schema::rename("ozekimessagein", "ozekimessageins");
        Schema::rename("ozekimessageout", "ozekimessageouts");
        Schema::rename("receieved_item", "receieved_items");
        Schema::rename("sms_balance", "sms_balances");
        Schema::rename("sms_transaction", "sms_transactions");
        Schema::rename("station", "stations");
        Schema::rename("subscriber", "subscribers");

        Schema::table('charges', function (Blueprint $table) {
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->nullable();
        });

        Schema::table('ozekimessageins', function (Blueprint $table) {
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->nullable();
        });

        Schema::table('ozekimessageouts', function (Blueprint $table) {
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->nullable();
        });

        Schema::table('receieved_items', function (Blueprint $table) {
        $table->timestamp('created_at')->useCurrent();
        $table->timestamp('updated_at')->nullable();
    });

        Schema::table('sms_balances', function (Blueprint $table) {
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->nullable();
            $table->renameColumn('sender_no', 'subscriber_id');
        });

        Schema::table('sms_transactions', function (Blueprint $table) {
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->nullable();
            $table->dropColumn('sender_no');
        });

        Schema::table('stations', function (Blueprint $table) {
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->nullable();
            $table->renameColumn('sender_no', 'subscriber_id');
        });

        Schema::table('subscribers', function (Blueprint $table) {
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->nullable();
            $table->integer('station_id');
            $table->dropColumn('station');
        });



        Schema::table('sms_balances', function(Blueprint $table) {
            $table->integer('subscriber_id')->unsigned()->change();
            $table->foreign('subscriber_id')->references('id')->on('subscribers')->onDelete('restrict')->onUpdate('cascade');
        });

        Schema::table('receieved_items', function(Blueprint $table) {
            $table->integer('user_id')->unsigned()->change();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('restrict')->onUpdate('cascade');
        });

        Schema::table('sms_transactions', function(Blueprint $table) {
            $table->integer('user_id')->unsigned()->change();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('restrict')->onUpdate('cascade');
        });
        Schema::table('stations', function(Blueprint $table) {
            $table->integer('subscriber_id')->unsigned()->change();
            $table->foreign('subscriber_id')->references('id')->on('subscribers')->onDelete('restrict')->onUpdate('cascade');
        });

        Schema::table('subscribers', function(Blueprint $table) {
            $table->integer('user_id')->unsigned()->change();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('restrict')->onUpdate('cascade');
        });
        Schema::table('subscribers', function(Blueprint $table) {
            $table->integer('station_id')->unsigned()->change();
            $table->foreign('station_id')->references('id')->on('stations')->onDelete('restrict')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('subscribers', function(Blueprint $table) {
            $table->dropForeign('station_id');
            $table->dropForeign('user_id');
        });
        Schema::table('stations', function(Blueprint $table) {
            $table->dropForeign('subscriber_id');
        });
        Schema::table('sms_transactions', function(Blueprint $table) {
            $table->dropForeign('user_id');
        });
        Schema::table('sms_balances', function(Blueprint $table) {
            $table->dropForeign('subscriber_id');
        });
        Schema::table('receieved_items', function(Blueprint $table) {
            $table->dropForeign('user_id');
        });

        Schema::table('subscribers', function (Blueprint $table) {
            $table->dropColumn('created_at');
            $table->dropColumn('updated_at');
            $table->dropColumn('station_id');
            $table->string('station');
        });

        Schema::table('stations', function (Blueprint $table) {
            $table->dropColumn('created_at');
            $table->dropColumn('updated_at');
            $table->renameColumn('subscriber_id', 'sender_no');
        });

        Schema::table('sms_transactions', function (Blueprint $table) {
            $table->dropColumn('created_at');
            $table->dropColumn('updated_at');
            $table->integer('senderno');
        });

        Schema::table('sms_balances', function (Blueprint $table) {
            $table->dropColumn('created_at');
            $table->dropColumn('updated_at');
            $table->renameColumn('subscriber_id', 'sender_no');
        });

        Schema::table('receieved_items', function (Blueprint $table) {
            $table->dropColumn('created_at');
            $table->dropColumn('updated_at');
        });

        Schema::table('ozekimessageouts', function (Blueprint $table) {
            $table->dropColumn('created_at');
            $table->dropColumn('updated_at');
        });

        Schema::table('ozekimessageins', function (Blueprint $table) {
            $table->dropColumn('created_at');
            $table->dropColumn('updated_at');
        });

        Schema::table('charges', function (Blueprint $table) {
            $table->dropColumn('created_at');
            $table->dropColumn('updated_at');
        });

        Schema::rename("subscribers", "subscriber");

        Schema::rename("stations", "station");

        Schema::rename("sms_transactions", "sms_transaction");

        Schema::rename("sms_balances", "sms_balance");

        Schema::rename("receieved_items", "receieved_item");

        Schema::rename("ozekimessageouts", "ozekimessageout");

        Schema::rename("ozekimessageins", "ozekimessagein");

        Schema::rename("charges", "charge");

    }
}
