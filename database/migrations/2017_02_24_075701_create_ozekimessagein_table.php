<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOzekimessageinTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        DB::beginTransaction();
        Schema::create('ozekimessagein', function (Blueprint $table) {
            $table->increments('id');
            $table->string('sender',50)->nullable();
            $table->string('receiver',50)->nullable();
            $table->string('msg',150)->nullable();
            $table->string('senttime',100)->nullable();
            $table->string('receivedtime',150)->nullable();
            $table->string('operator',150)->nullable();
            $table->string('msgtype',150)->nullable();
            $table->string('reference',150)->nullable();
            $table->string('isprocessed',150)->nullable();
        });

         Schema::create('ozekimessageout', function (Blueprint $table) {
            $table->increments('id');
            $table->string('sender',50)->nullable();
            $table->string('receiver',50)->nullable();
            $table->string('msg',150)->nullable();
            $table->string('senttime',100)->nullable();
            $table->string('receivedtime',150)->nullable();
            $table->string('operator',150)->nullable();
            $table->string('msgtype',30)->nullable();
            $table->string('reference',30)->nullable();
            $table->string('status',30)->nullable();
            $table->string('errormsg',150)->nullable();
        });

        Schema::create('receieved_item', function (Blueprint $table) {
            $table->increments('id');
            $table->string('box_no',50);
            $table->string('received_from',50)->nullable();
            $table->integer('status_flag')->default("0");
            $table->date('date_received');
            $table->string('user_id',150);

        });

        Schema::create('sms_balance', function (Blueprint $table) {
            $table->increments('id');
            $table->float('remain_balance');
            $table->date('date_topup');
            $table->integer('sender_no');

        });

        Schema::create('sms_transaction', function (Blueprint $table) {
            $table->increments('id');
            $table->float('credit')->nullable();
            $table->float('debit')->nullable();
            $table->integer('balance');
            $table->date('date_topup')->nullable();
            $table->string('user_id',50);
            $table->integer('sender_no');

        });


        Schema::create('station', function (Blueprint $table) {
            $table->increments('id');
            $table->string('station_name',70);
            $table->integer('sender_no');
            $table->integer('total_boxes');
            $table->date('password');

        });


        Schema::create('subscriber', function (Blueprint $table) {
            $table->increments('id');
            $table->string('other_names',70);
            $table->string('last_name',70);
            $table->integer('contact_1');
            $table->integer('contact_2')->nullable();
            $table->string('address',150)->nullable();
            $table->integer('box_no');
            $table->string('email',150)->nullable();
            $table->integer('paystatus_flag')->default(0);
            $table->date('paydate')->nullable();
            $table->float('credit_balance')->default(0);
            $table->float('last_amount_paid')->nullable();
            $table->string('user_id',150);
            $table->integer('sender_no');
            $table->integer('sms_reminder_count')->default(0);
            $table->string('station');
            $table->date('expire_date');

        });

        Schema::create('charge', function (Blueprint $table) {
            $table->increments('id');
            $table->string('charge');

        });

        DB::commit();

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('charge');
        Schema::dropIfExists('subscriber');
        Schema::dropIfExists('station');
        Schema::dropIfExists('sms_transaction');
        Schema::dropIfExists('sms_balance');
        Schema::dropIfExists('received_item');
        Schema::dropIfExists('ozekimessagein');
        Schema::dropIfExists('ozekimessageout');
    }
}
